package ml.antanaks.school.base;

import java.text.NumberFormat;
import java.util.Locale;

public class StringOperations {

    public static int getSummaryLength(String[] strings) {
        int result = 0;
        for (String str : strings)
            result = result + str.length();
        return result;
    }

    public static String getFirstAndLastLetterString(String string) {
        return String.valueOf(string.charAt(0)) + string.charAt(string.length() - 1);
    }

    public static boolean isSameCharAtPosition(String string1, String string2, int index) {
        return string1.charAt(index) == string2.charAt(index);
    }

    public static boolean isSameFirstCharPosition(String string1, String string2, char character) {
        for (int i = 0; i < string1.length(); i++)
            if (string1.charAt(i) == character && string2.charAt(i) == character)
                return true;
        return false;
    }

    public static boolean isSameLastCharPosition(String string1, String string2, char character) {
        for (int i = string1.length() - 1; i >= 0; i--)
            if (string1.charAt(i) == character && string2.charAt(i) == character)
                return true;
        return false;
    }

    public static boolean isSameFirstStringPosition(String string1, String string2, String str) {
        for (int i = 0; i < string1.length(); i++)
            if (string1.startsWith(str, i))
                return hasSameSubstring(string1, string2, i, str.length());
        return false;
    }

    public static boolean isSameLastStringPosition(String string1, String string2, String str) {
        for (int i = string1.length(); i >= 0; i--)
            if (string1.startsWith(str, i - str.length()))
                return hasSameSubstring(string1, string2, i - str.length(), str.length());
        return false;
    }

    public static boolean isEqual(String string1, String string2) {
        return string1.equals(string2);
    }

    public static boolean isEqualIgnoreCase(String string1, String string2) {
        return string1.equalsIgnoreCase(string2);
    }

    public static boolean isLess(String string1, String string2) {
        return string1.compareTo(string2) < 0;
    }

    public static boolean isLessIgnoreCase(String string1, String string2) {
        return string1.compareToIgnoreCase(string2) < 0;
    }

    public static String concat(String string1, String string2) {
        return string1 + string2;
    }

    public static boolean isSamePrefix(String string1, String string2, String prefix) {
        return string1.startsWith(prefix) && string2.startsWith(prefix);
    }

    public static boolean isSameSuffix(String string1, String string2, String suffix) {
        return string1.endsWith(suffix) && string2.endsWith(suffix);
    }


    public static String getCommonPrefix(String string1, String string2) {
        String s = "";
        for (int i = 0; i < Math.min(string1.length(), string2.length()); i++) {
            if (string1.charAt(i) == string2.charAt(i)) {
                s = string1.substring(0, i + 1);
            } else {
                break;
            }
        }
        return s;
    }

    public static String reverse(String string) {
        return new StringBuffer(string).reverse().toString();
    }

    public static boolean isPalindrome(String string) {
        return string.equals(reverse(string));
    }

    public static boolean isPalindromeIgnoreCase(String string) {
        return string.equalsIgnoreCase(reverse(string));
    }

    public static String getLongestPalindromeIgnoreCase(String[] strings) {
        int index = 0;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < strings.length; i++) {
            if (isPalindromeIgnoreCase(strings[i]))
                if (max < strings[i].length()) {
                    max = strings[i].length();
                    index = i;
                }
        }
        return strings[index];
    }

    public static boolean hasSameSubstring(String string1, String string2, int index, int length) {
        int end = index + length;
        if (string1.length() < end || string2.length() < end) return false;
        return string1.substring(index, end).equals(string2.substring(index, end));
    }

    public static boolean isEqualAfterReplaceCharacters(String string1, char replaceInStr1, char replaceByInStr1,
                                                        String string2, char replaceInStr2, char replaceByInStr2) {
        return string1.replace(replaceInStr1, replaceByInStr1)
                .equals(string2.replace(replaceInStr2, replaceByInStr2));
    }

    public static boolean isEqualAfterReplaceStrings(String string1, String replaceInStr1, String replaceByInStr1,
                                                     String string2, String replaceInStr2, String replaceByInStr2) {
        return string1.replaceAll(replaceInStr1, replaceByInStr1)
                .equals(string2.replace(replaceInStr2, replaceByInStr2));
    }


    public static boolean isPalindromeAfterRemovingSpacesIgnoreCase(String string) {
        StringBuilder stringBuilder = new StringBuilder(string);
        int count = 0;
        for (int i = 0; i < string.length(); i++)
            if (string.charAt(i) == ' ') {
                stringBuilder.deleteCharAt(i - count);
                count++;
            }
        return isPalindromeIgnoreCase(stringBuilder.toString());
    }

    public static boolean isEqualAfterTrimming(String string1, String string2) {
        return isEqual(string1.trim(), string2.trim());
    }

    public static String makeCsvStringFromInts(int[] array) {
        return makeCsvStringBuilderFromInts(array).toString();
    }

    public static String makeCsvStringFromDoubles(double[] array) {
        return makeCsvStringBuilderFromDoubles(array).toString();
    }

    public static StringBuilder makeCsvStringBuilderFromInts(int[] array) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            sb.append(array[i]);
            if (i != array.length - 1) sb.append(',');
        }
        return sb;
    }

    public static StringBuilder makeCsvStringBuilderFromDoubles(double[] array) {
        StringBuilder sb = new StringBuilder();
        NumberFormat f = NumberFormat.getNumberInstance(Locale.ENGLISH);
        f.setMaximumFractionDigits(2);
        f.setMinimumFractionDigits(2);
        for (int i = 0; i < array.length; i++) {
            sb.append(f.format(array[i]));
            if (i != array.length - 1) sb.append(",");
        }
        return sb;
    }

    public static StringBuilder removeCharacters(String string, int[] positions) {
        StringBuilder sb = new StringBuilder(string);
        for (int i = 0; i < positions.length; i++)
            sb.deleteCharAt(positions[i] - i);
        return sb;
    }

    public static StringBuilder insertCharacters(String string, int[] positions, char[] characters) {
        StringBuilder sb = new StringBuilder(string);
        for (int i = 0; i < positions.length; i++) {
            sb.insert(positions[i] + i, characters[i]);
        }
        return sb;
    }
}
