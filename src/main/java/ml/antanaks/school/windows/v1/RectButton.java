package ml.antanaks.school.windows.v1;

import java.util.Objects;

public class RectButton {

    private Point topLeft;
    private Point bottomRight;
    private boolean active;


    public RectButton(Point topLeft, Point bottomRight, boolean active){
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
        this.active = active;
    }

    public RectButton(int xLeft, int yTop, int width, int height, boolean active){
        this(new Point(xLeft, yTop),new Point(xLeft+width-1, yTop+height-1), active);
    }
    public RectButton(Point topLeft, Point bottomRight){
        this(topLeft, bottomRight, true);
    }

    public RectButton(int xLeft, int yTop, int width, int height){
        this(new Point(xLeft, yTop), new Point(xLeft+width-1, yTop + height-1), true);
    }

    public Point getTopLeft() {
        return topLeft;
    }

    public Point getBottomRight() {
        return bottomRight;
    }

    public boolean isActive() {
        return active;
    }

    public void setTopLeft(Point topLeft) {
        this.topLeft = topLeft;
    }

    public void setBottomRight(Point bottomRight) {
        this.bottomRight = bottomRight;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getWidth(){
        return this.bottomRight.getX()-this.topLeft.getX()+1;
    }
    public int getHeight(){
        return this.bottomRight.getY()-this.topLeft.getY()+1;
    }

    public void moveTo(int x, int y){
        int width = getWidth() - 1;
        int height = getHeight() - 1;
        topLeft.moveTo(x, y);
        bottomRight.moveTo(topLeft.getX()+width, topLeft.getY()+height);
    }

    public void moveTo(Point point){
        this.moveTo(point.getX(), point.getY());
    }

    public void moveRel(int dx, int dy){
        int width = getWidth() -1;
        int height = getHeight() -1;
        topLeft.moveTo(topLeft.getX()+dx, topLeft.getY()+dy);
        bottomRight.moveTo(topLeft.getX()+width, topLeft.getY()+height);
    }

    public void resize(double ratio){
        int width = (int) (getWidth()*ratio);
        int height = (int) (getHeight()*ratio);
        if(width<1||height<1){
            bottomRight.setX(topLeft.getX());
            bottomRight.setY(topLeft.getY());
        }else {
            bottomRight.setX(topLeft.getX()+width-1);
            bottomRight.setY(topLeft.getY()+height-1);
        }
    }

    public boolean isInside(int x, int y){
        return (x>= topLeft.getX() &&
                y>=topLeft.getY() &&
                x<=bottomRight.getX()&&
                y<=bottomRight.getY());
    }
    public boolean isInside(Point point){
        return isInside(point.getX(), point.getY());
    }

    public boolean isIntersects(RectButton rectButton){
        return this.isInside(rectButton.getTopLeft())
                || this.isInside(rectButton.getBottomRight())
                || rectButton.isInside(this.getTopLeft())
                || rectButton.isInside(this.getBottomRight());
    }
    public boolean isInside(RectButton rectButton){
        return (this.isInside(rectButton.getTopLeft())
                &&this.isInside(rectButton.getBottomRight())
                || rectButton.isInside(this.getTopLeft())
                && rectButton.isInside(this.getBottomRight()));
    }

    public boolean isFullyVisibleOnDesktop(Desktop desktop){
        return (getTopLeft().getX() >=0
        && getBottomRight().getX()<=desktop.getWidth())
                &&(getTopLeft().getY()>=0
        && getBottomRight().getY()<= desktop.getHeight());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RectButton that = (RectButton) o;
        return active == that.active && topLeft.equals(that.topLeft) && bottomRight.equals(that.bottomRight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topLeft, bottomRight, active);
    }
}
