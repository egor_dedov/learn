package ml.antanaks.school.windows.v1;

import java.util.Objects;

public class RoundButton {
    private Point center;
    private int radius;
    private boolean active;

    public RoundButton(Point center, int radius) {
        this(center, radius, true);
    }

    public RoundButton(int xCenter, int yCenter, int radius, boolean active) {
        this(new Point(xCenter,yCenter), radius,active);
    }

    public RoundButton(Point center, int radius, boolean active) {
        this.center = center;
        this.radius = radius;
        this.active = active;
    }

    public RoundButton(int xCenter, int yCenter, int radius) {
        this(new Point(xCenter,yCenter), radius, true);
    }

    public Point getCenter() {
        return center;
    }

    public int getRadius() {
        return radius;
    }

    public boolean isActive() {
        return active;
    }

    public void moveTo(int x, int y) {
        center.setX(x);
        center.setY(y);
    }

    public void moveTo(Point point) {
        center = point;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public void setCenter(int x, int y) {
        this.center = new Point(x, y);
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void moveRel(int dx, int dy) {
        this.center.setX(center.getX() + dx);
        this.center.setY(center.getY() + dy);
    }

    public void resize(double ratio) {
        int radius1 = (int) (getRadius()*ratio);
        radius = Math.max(radius1, 1);
    }

    public boolean isInside(int x, int y) {
       double dis = Math.pow((center.getX()-x), 2)+Math.pow(center.getY()-y, 2);
       return ( (int) Math.sqrt(dis)) <=radius;
    }

    public boolean isInside(Point point) {
        double dis = Math.pow(center.getX()- point.getX(), 2)+ Math.pow(center.getY()- point.getY(), 2);
        return ((int) Math.sqrt(dis)) <= radius;
    }

    public boolean isFullyVisibleOnDesktop(Desktop desktop) {
        return ((center.getX()-radius)>=0
        &&(center.getX()+radius)<desktop.getWidth())
        &&((center.getY()-radius)>=0
                &&(center.getY()+radius)<desktop.getHeight());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoundButton that = (RoundButton) o;
        return radius == that.radius && active == that.active && center.equals(that.center);
    }

    @Override
    public int hashCode() {
        return Objects.hash(center, radius, active);
    }
}