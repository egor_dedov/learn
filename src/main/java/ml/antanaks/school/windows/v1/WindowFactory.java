package ml.antanaks.school.windows.v1;

public class WindowFactory {

    static int rectButtonCount;
    static int roundButtonCount;
    static int allButtonCount;

    public static RectButton createRectButton(Point leftTop, Point rightBottom, boolean active){
        RectButton rectButton = new RectButton(leftTop, rightBottom, active);
        rectButtonCount++;
        allButtonCount++;
        return rectButton;
    }

    public static RoundButton createRoundButton(Point center, int radius, boolean active){
        RoundButton roundButton = new RoundButton(center, radius, active);
        roundButtonCount++;
        allButtonCount++;
        return roundButton;
    }

    public static int getRectButtonCount(){
         return rectButtonCount;
    }


    public static int getRoundButtonCount(){
       return roundButtonCount;
    }

    public static int getWindowCount(){
        return allButtonCount;
    }
    public static void reset(){
        allButtonCount = 0;
    }
}
