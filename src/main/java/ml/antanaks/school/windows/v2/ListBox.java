package ml.antanaks.school.windows.v2;

import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

public class ListBox {

    private Point topLeft;
    private Point bottomRight;
    private boolean active;
    private String[] lines;


    public ListBox(Point topLeft, Point bottomRight, boolean active, String[] lines) {
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
        this.active = active;
        this.lines = lines;
    }


    public ListBox(int xLeft, int yTop, int width, int height, boolean active, String[] lines) {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), active, lines);
    }

    public ListBox(Point topLeft, Point bottomRight, String[] lines) {
        this(topLeft, bottomRight, true, lines);
    }

    public ListBox(int xLeft, int yTop, int width, int height, String[] lines) {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), true, lines);
    }

    public Point getTopLeft() {
        return topLeft;
    }

    public void setTopLeft(Point topLeft) {
        this.topLeft = topLeft;
    }

    public Point getBottomRight() {
        return bottomRight;
    }

    public void setBottomRight(Point bottomRight) {
        this.bottomRight = bottomRight;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getWidth() {
        return bottomRight.getX() - topLeft.getX() + 1;
    }

    public int getHeight() {
        return bottomRight.getY() - topLeft.getY() + 1;
    }

    public String[] getLines() {
        if (lines != null) {
            String[] arrayDestination = new String[lines.length];
            System.arraycopy(lines, 0, arrayDestination, 0, lines.length);
            return arrayDestination;
        }
        return null;
    }

    public void setLines(String[] lines) {
        this.lines = lines;
    }

    public String[] getLinesSlice(int from, int to) {
        if (lines != null) {
            int to1 = 0;
            String[] arrayDestination = new String[to - from - 1];
            if (this.lines.length >= to) {
                arrayDestination = Arrays.copyOfRange(this.lines, from, to);
            } else {
                to1 = this.lines.length;
                arrayDestination = Arrays.copyOfRange(this.lines, from, to1);
            }
            return arrayDestination;
        }
        return null;
    }


    public String getLine(int index) {
        if (lines != null && lines.length > index) {
            return lines[index];
        }
        return null;
    }


    public void setLine(int index, String line) {
        if (lines != null || lines.length >= index) {
            String newArray[] = new String[lines.length];
            System.arraycopy(lines, 0, newArray, 0, lines.length);
            newArray[index] = line;
            lines = newArray;
        }
    }

    public Integer findLine(String line) {
        if (lines != null) {
            for (int i = 0; i < lines.length; i++) {
                if (lines[i].equals(line)) {
                    return i;
                }
            }
        }
        return null;
    }

    public void reverseLineOrder() {
        if (lines != null) {
            Collections.reverse(Arrays.asList(lines));
        }
    }

    public void reverseLines() {
        if (lines != null) {
            for (int i = 0; i < lines.length; i++) {
                lines[i] = new StringBuilder(lines[i]).reverse().toString();
            }
        }
    }

    public void duplicateLines() {
        if (lines != null) {
            String[] stringDestination = new String[lines.length * 2];
            int j = 0;
            for (int i = 0; i < stringDestination.length; i = i + 2) {
                stringDestination[i] = lines[j];
                stringDestination[i + 1] = lines[j];
                j++;
            }
            lines = stringDestination;
        }
    }

    public void removeOddLines() {
        if (lines != null) {
            int k;
            if (lines.length % 2 == 0) {
                k = lines.length / 2;
            } else {
                k = lines.length / 2 + 1;
            }
            String[] newArray = new String[k];
            int j = 0;
            for (int i = 0; i < lines.length; i += 2) {
                newArray[j] = lines[i];
                j++;
            }
            lines = newArray;
        }
    }

    public boolean isSortedDescendant() {
        if (lines != null) {
            for (int i = 0; i < lines.length - 1; i++) {
                if (lines[i].compareTo(lines[i + 1]) > 0) {
                    return true;
                }
            }
        } else if (lines == null) {
            return true;
        }
        return false;
    }

    public void moveTo(int x, int y) {
        int width = getWidth() - 1;
        int height = getHeight() - 1;
        topLeft.moveTo(x, y);
        bottomRight.moveTo(topLeft.getX() + width, topLeft.getY() + height);
    }

    public void moveTo(Point point) {
        this.moveTo(point.getX(), point.getY());
    }


    public void moveRel(int dx, int dy) {
        int width = getWidth() - 1;
        int height = getHeight() - 1;
        topLeft.moveTo(topLeft.getX() + dx, topLeft.getY() + dy);
        bottomRight.moveTo(topLeft.getX() + width, topLeft.getY() + height);
    }

    public void resize(double ratio) {
        int width = (int) (getWidth() * ratio);
        int height = (int) (getHeight() * ratio);
        if (width < 1 || height < 1) {
            bottomRight.setX(topLeft.getX());
            bottomRight.setY(topLeft.getY());
        } else {
            bottomRight.setX(topLeft.getX() + width - 1);
            bottomRight.setY(topLeft.getY() + height - 1);
        }
    }

    public boolean isInside(int x, int y) {
        return (x >= topLeft.getX() &&
                y >= topLeft.getY() &&
                x <= bottomRight.getX() &&
                y <= bottomRight.getY());
    }

    public boolean isInside(Point point) {
        return isInside(point.getX(), point.getY());
    }

    public boolean isIntersects(ListBox listBox) {
        return isInside(listBox.getTopLeft())
                || isInside(listBox.getBottomRight())
                || listBox.isInside(getTopLeft())
                || listBox.isInside(getBottomRight());
    }

    public boolean isInside(ListBox listBox) {
        return (isInside(listBox.getTopLeft())
                && isInside(listBox.getBottomRight())
                || listBox.isInside(getTopLeft())
                && listBox.isInside(getBottomRight()));
    }

    public boolean isFullyVisibleOnDesktop(Desktop desktop) {
        return (getTopLeft().getX() >= 0
                && getBottomRight().getX() <= desktop.getWidth())
                && (getTopLeft().getY() >= 0
                && getBottomRight().getY() <= desktop.getHeight());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListBox listBox = (ListBox) o;
        return active == listBox.active && topLeft.equals(listBox.topLeft) && bottomRight.equals(listBox.bottomRight) && Arrays.equals(lines, listBox.lines);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(topLeft, bottomRight, active);
        result = 31 * result + Arrays.hashCode(lines);
        return result;
    }
}
