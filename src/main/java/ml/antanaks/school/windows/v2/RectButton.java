package ml.antanaks.school.windows.v2;

import java.util.Objects;

public class RectButton {

    private Point topLeft;
    private Point bottomRight;
    private boolean active;
    private String text;


    public RectButton(Point topLeft, Point bottomRight, boolean active, String text) {
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
        this.active = active;
        this.text = text;
    }

    public RectButton(int xLeft, int yTop, int width, int height, boolean active, String text) {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), active, text);
    }

    public RectButton(Point topLeft, Point bottomRight, String text) {
        this(topLeft, bottomRight, true, text);
    }

    public RectButton(int xLeft, int yTop, int width, int height, String text) {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), true, text);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Point getTopLeft() {
        return topLeft;
    }

    public Point getBottomRight() {
        return bottomRight;
    }

    public boolean isActive() {
        return active;
    }

    public void setTopLeft(Point topLeft) {
        this.topLeft = topLeft;
    }

    public void setBottomRight(Point bottomRight) {
        this.bottomRight = bottomRight;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getWidth() {
        return bottomRight.getX() - topLeft.getX() + 1;
    }

    public int getHeight() {
        return bottomRight.getY() - topLeft.getY() + 1;
    }

    public void moveTo(int x, int y) {
        int width = getWidth() - 1;
        int height = getHeight() - 1;
        topLeft.moveTo(x, y);
        bottomRight.moveTo(topLeft.getX() + width, topLeft.getY() + height);
    }

    public void moveTo(Point point) {
        moveTo(point.getX(), point.getY());
    }

    public void moveRel(int dx, int dy) {
        int width = getWidth() - 1;
        int height = getHeight() - 1;
        topLeft.moveTo(topLeft.getX() + dx, topLeft.getY() + dy);
        bottomRight.moveTo(topLeft.getX() + width, topLeft.getY() + height);
    }

    public void resize(double ratio) {
        int width = (int) (getWidth() * ratio);
        int height = (int) (getHeight() * ratio);
        if (width < 1 || height < 1) {
            bottomRight.setX(topLeft.getX());
            bottomRight.setY(topLeft.getY());
        } else {
            bottomRight.setX(topLeft.getX() + width - 1);
            bottomRight.setY(topLeft.getY() + height - 1);
        }
    }

    public boolean isInside(int x, int y) {
        return (x >= topLeft.getX() &&
                y >= topLeft.getY() &&
                x <= bottomRight.getX() &&
                y <= bottomRight.getY());
    }

    public boolean isInside(Point point) {
        return isInside(point.getX(), point.getY());
    }

    public boolean isIntersects(RectButton rectButton) {
        return isInside(rectButton.getTopLeft())
                || isInside(rectButton.getBottomRight())
                || rectButton.isInside(getTopLeft())
                || rectButton.isInside(getBottomRight());
    }

    public boolean isInside(RectButton rectButton) {
        return (isInside(rectButton.getTopLeft())
                && isInside(rectButton.getBottomRight())
                || rectButton.isInside(getTopLeft())
                && rectButton.isInside(getBottomRight()));
    }

    public boolean isFullyVisibleOnDesktop(Desktop desktop) {
        return (getTopLeft().getX() >= 0
                && getBottomRight().getX() <= desktop.getWidth())
                && (getTopLeft().getY() >= 0
                && getBottomRight().getY() <= desktop.getHeight());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RectButton that = (RectButton) o;
        return active == that.active && topLeft.equals(that.topLeft) && bottomRight.equals(that.bottomRight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topLeft, bottomRight, active);
    }

}
