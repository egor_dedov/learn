package ml.antanaks.school.windows.v3;

public class RadioButton extends RoundButton {

    public RadioButton(Point center, int radius, boolean active, String text, boolean checked) {
        super(center, radius, active, text);
        this.setChecked(checked);
    }

    public RadioButton(int xCenter, int yCenter, int radius, boolean active, String text, boolean checked) {
        this(new Point(xCenter, yCenter), radius, active, text, checked);
    }

    public RadioButton(Point center, int radius, String text, boolean checked) {
        this(center, radius, true, text, checked);
    }

    public RadioButton(int xCenter, int yCenter, int radius, String text, boolean checked) {
        this(new Point(xCenter, yCenter), radius, true, text, checked);
    }
}
