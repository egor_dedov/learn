package ml.antanaks.school.windows.v3;

import ml.antanaks.school.windows.v3.base.RectWindow;

public class RectButton extends RectWindow {

    public RectButton(Point topLeft, Point bottomRight, boolean active, String text) {
        this.setTopLeft(topLeft);
        this.setBottomRight(bottomRight);
        this.setActive(active);
        this.setText(text);
    }

    public RectButton(int xLeft, int yTop, int width, int height, boolean active, String text) {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), active, text);
    }

    public RectButton(Point topLeft, Point bottomRight, String text) {
        this(topLeft, bottomRight, true, text);
    }

    public RectButton(int xLeft, int yTop, int width, int height, String text) {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), true, text);
    }

    public boolean isInside(int x, int y) {
        return (x >= getTopLeft().getX() &&
                y >= getTopLeft().getY() &&
                x <= getBottomRight().getX() &&
                y <= getBottomRight().getY());
    }

    public boolean isInside(Point point) {
        return isInside(point.getX(), point.getY());
    }

    public boolean isIntersects(RectButton rectButton) {
        return isInside(rectButton.getTopLeft())
                || isInside(rectButton.getBottomRight())
                || rectButton.isInside(getTopLeft())
                || rectButton.isInside(getBottomRight());
    }

    public boolean isInside(RectButton rectButton) {
        return (isInside(rectButton.getTopLeft())
                && isInside(rectButton.getBottomRight())
                || rectButton.isInside(getTopLeft())
                && rectButton.isInside(getBottomRight()));
    }

    public boolean isFullyVisibleOnDesktop(Desktop desktop) {
        return (getTopLeft().getX() >= 0
                && getBottomRight().getX() <= desktop.getWidth())
                && (getTopLeft().getY() >= 0
                && getBottomRight().getY() <= desktop.getHeight());
    }

}
