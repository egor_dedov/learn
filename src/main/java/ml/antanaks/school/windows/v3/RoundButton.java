package ml.antanaks.school.windows.v3;

import ml.antanaks.school.windows.v3.base.RoundWindow;

public class RoundButton extends RoundWindow {

    public RoundButton(Point center, int radius, String text) {
        this(center, radius, true, text);
    }

    public RoundButton(int xCenter, int yCenter, int radius, boolean active, String text) {
        this(new Point(xCenter, yCenter), radius, active, text);
    }

    public RoundButton(Point center, int radius, boolean active, String text) {
        this.setCenter(center);
        this.setRadius(radius);
        this.setActive(active);
        this.setText(text);
    }

    public RoundButton(int xCenter, int yCenter, int radius, String text) {
        this(new Point(xCenter, yCenter), radius, true, text);
    }
}