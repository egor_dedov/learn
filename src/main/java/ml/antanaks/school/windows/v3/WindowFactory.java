package ml.antanaks.school.windows.v3;

public class WindowFactory {

    static int rectButtonCount;
    static int roundButtonCount;


    public static RectButton createRectButton(Point leftTop, Point rightBottom, boolean active, String text) { //уменьши количество строк кода на 2
        RectButton rectButton = new RectButton(leftTop, rightBottom, active, text);
        rectButtonCount++;
        return rectButton;

    }

    public static RoundButton createRoundButton(Point center, int radius, boolean active, String text) {//уменьши количество строк кода на 2
        RoundButton roundButton = new RoundButton(center, radius, active, text);
        roundButtonCount++;
        return roundButton;
    }

    public static int getRectButtonCount() {
        return rectButtonCount;
    }


    public static int getRoundButtonCount() {
        return roundButtonCount;
    }

    public static int getWindowCount() {
        return rectButtonCount + roundButtonCount;
    }

    public static void reset() {
        rectButtonCount = 0;
        roundButtonCount = 0;
    }
}
