package ml.antanaks.school.windows.v3.base;

import ml.antanaks.school.windows.v3.Point;

import java.util.Objects;

public abstract class RectWindow extends Window {

    private Point topLeft;
    private Point bottomRight;
    private int zHeight;

    public int getZHeight() {
        return zHeight;
    }

    public void setZHeight(int zHeight) {
        this.zHeight = zHeight;
    }

    public Point getTopLeft() {
        return topLeft;
    }

    public Point getBottomRight() {
        return bottomRight;
    }

    public void setTopLeft(Point topLeft) {
        this.topLeft = topLeft;
    }

    public void setBottomRight(Point bottomRight) {
        this.bottomRight = bottomRight;
    }

    public int getWidth() {
        return bottomRight.getX() - topLeft.getX() + 1;
    }

    public int getHeight() {
        return bottomRight.getY() - topLeft.getY() + 1;
    }

    @Override
    public void moveTo(int x, int y) {
        int width = getWidth() - 1;
        int height = getHeight() - 1;
        topLeft.moveTo(x, y);
        bottomRight.moveTo(topLeft.getX() + width, topLeft.getY() + height);
    }

    @Override
    public void moveRel(int dx, int dy) {
        int width = getWidth() - 1;
        int height = getHeight() - 1;
        topLeft.moveTo(topLeft.getX() + dx, topLeft.getY() + dy);
        bottomRight.moveTo(topLeft.getX() + width, topLeft.getY() + height);
    }

    @Override
    public void resize(double ratio) {
        int width = (int) (getWidth() * ratio);
        int height = (int) (getHeight() * ratio);
        if (width < 1 || height < 1) {
            bottomRight.setX(topLeft.getX());
            bottomRight.setY(topLeft.getY());
        } else {
            bottomRight.setX(topLeft.getX() + width - 1);
            bottomRight.setY(topLeft.getY() + height - 1);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RectWindow)) return false;
        if (!super.equals(o)) return false;
        RectWindow that = (RectWindow) o;
        return zHeight == that.zHeight && topLeft.equals(that.topLeft) && bottomRight.equals(that.bottomRight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), topLeft, bottomRight, zHeight);
    }
}
