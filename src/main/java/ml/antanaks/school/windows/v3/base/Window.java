package ml.antanaks.school.windows.v3.base;

import ml.antanaks.school.windows.v3.Desktop;
import ml.antanaks.school.windows.v3.Point;
import ml.antanaks.school.windows.v3.iface.Movable;
import ml.antanaks.school.windows.v3.iface.Resizable;

import java.util.Objects;

public abstract class Window implements Movable, Resizable {

    private boolean active;
    private String text;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public abstract boolean isInside(int x, int y);

    public abstract boolean isInside(Point point);

    public abstract boolean isFullyVisibleOnDesktop(Desktop desktop);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Window)) return false;
        Window window = (Window) o;
        return active == window.active && text.equals(window.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(active, text);
    }
}
