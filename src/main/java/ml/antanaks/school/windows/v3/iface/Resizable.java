package ml.antanaks.school.windows.v3.iface;

public interface Resizable {

    void resize(double ratio);

}
