package ml.antanaks.school.windows.v4;

import ml.antanaks.school.windows.v4.base.WindowErrorCode;
import ml.antanaks.school.windows.v4.base.WindowException;
import ml.antanaks.school.windows.v4.base.WindowState;

import java.util.Objects;

public class ComboBox extends ListBox {

    private Integer selected;

    public ComboBox(Point topLeft, Point bottomRight, WindowState state, String[] lines, Integer selected) throws WindowException {
        super(topLeft, bottomRight, state, lines);
        this.selected = selected;
        if (lines == null & selected != null) {
            throw new WindowException(WindowErrorCode.EMPTY_ARRAY);
        }
        if (selected != null) {
            if (selected > lines.length - 1) {
                throw new WindowException(WindowErrorCode.WRONG_INDEX);
            }
        }
        if (state == null) {
            throw new WindowException(WindowErrorCode.WRONG_STATE);
        }
        if (selected != null) {
            if (selected < 0) {
                throw new WindowException(WindowErrorCode.WRONG_INDEX);
            }
        }
    }

    public ComboBox(Point topLeft, Point bottomRight, String state, String[] lines, Integer selected) throws WindowException {
        this(topLeft, bottomRight, WindowState.fromString(state), lines, selected);
    }

    public ComboBox(int xLeft, int yTop, int width, int height, WindowState state, String[] lines, Integer selected) throws WindowException {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), state, lines, selected);
    }

    public ComboBox(int xLeft, int yTop, int width, int height, String state, String[] lines, Integer selected) throws WindowException {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), WindowState.fromString(state), lines, selected);
    }

    public ComboBox(Point topLeft, Point bottomRight, String[] lines, Integer selected) throws WindowException {
        this(topLeft, bottomRight, WindowState.ACTIVE, lines, selected);
    }

    public ComboBox(int xLeft, int yTop, int width, int height, String[] lines, Integer selected) throws WindowException {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), WindowState.ACTIVE, lines, selected);
    }

    public Integer getSelected() {
        if (getLines() == null) {
            selected = null;
        }
        return selected;
    }

    public void setSelected(Integer selected) throws WindowException {
        if (getLines() == null && getSelected() != null) {
            throw new WindowException(WindowErrorCode.EMPTY_ARRAY);
        }
        if (selected != null) {
            if (selected < 0 | selected > getLines().length - 1) {
                throw new WindowException(WindowErrorCode.WRONG_INDEX);
            }
        }
        this.selected = selected;
    }

    @Override
    public String getLine(int number) throws WindowException {
        if (getLines() == null) {
            throw new WindowException(WindowErrorCode.EMPTY_ARRAY);
        } else if (getLines().length <= number|number < 0) {
            throw new WindowException(WindowErrorCode.WRONG_INDEX);
        } else {
            return getLines()[number];
        }
    }

    @Override
    public void setLine(int index, String line) throws WindowException {
        if (getLines() == null) {
            throw new WindowException(WindowErrorCode.EMPTY_ARRAY);
        } else if (getLines().length <= index|index < 0) {
            throw new WindowException(WindowErrorCode.WRONG_INDEX);
        } else {
            String newArray[] = new String[getLines().length]; // создавай в одном стиле массивы
            System.arraycopy(getLines(), 0, newArray, 0, getLines().length);
            newArray[index] = line;
            setLines(newArray); //нах так сложно?
        }
    }

    public void setLines(String[] lines) {
        super.setLines(lines);
        selected = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ComboBox)) return false;
        if (!super.equals(o)) return false;
        ComboBox comboBox = (ComboBox) o;
        return selected.equals(comboBox.selected);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), selected);
    }
}
