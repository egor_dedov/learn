package ml.antanaks.school.windows.v4;

import ml.antanaks.school.windows.v4.base.*;

import java.util.Arrays;
import java.util.Collections;

public class ListBox extends RectWindow {

    private String[] lines;

    public ListBox(Point topLeft, Point bottomRight, WindowState state, String[] lines) throws WindowException {
        super(topLeft, bottomRight, state);
        this.lines = lines;
        if (state == null) {
            throw new WindowException(WindowErrorCode.WRONG_STATE);
        }
        if (state == WindowState.DESTROYED) {
            throw new WindowException(WindowErrorCode.WRONG_STATE);
        }
    }

    public ListBox(Point topLeft, Point bottomRight, String state, String[] lines) throws WindowException {
        this(topLeft, bottomRight, WindowState.fromString(state), lines);
    }

    public ListBox(int xLeft, int yTop, int width, int height, WindowState state, String[] lines) throws WindowException {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), state, lines);
    }

    public ListBox(int xLeft, int yTop, int width, int height, String state, String[] lines) throws WindowException {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), WindowState.fromString(state), lines);
    }

    public ListBox(Point topLeft, Point bottomRight, String[] lines) throws WindowException {
        this(topLeft, bottomRight, WindowState.ACTIVE, lines);
    }

    public ListBox(int xLeft, int yTop, int width, int height, String[] lines) throws WindowException {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), WindowState.ACTIVE, lines);
    }

    public String[] getLines() {
        if (lines != null) {
            String[] arrayDestination = new String[lines.length];
            System.arraycopy(lines, 0, arrayDestination, 0, lines.length);
            return arrayDestination;
        }
        return null;
    }

    public void setLines(String[] lines) {
        this.lines = lines;
    }

    public String[] getLinesSlice(int from, int to) throws WindowException {
        if (lines == null) {
            throw new WindowException(WindowErrorCode.EMPTY_ARRAY);
        } else if (from < 0 || lines.length < to || (from > to - 1)) {
            throw new WindowException(WindowErrorCode.WRONG_INDEX);
        } else{
            int to1 = 0;
            String[] arrayDestination;
            if (this.lines.length >= to) {
                arrayDestination = Arrays.copyOfRange(this.lines, from, to);
            } else {
                to1 = this.lines.length;
                arrayDestination = Arrays.copyOfRange(this.lines, from, to1);
            }
            return arrayDestination;
        }
    }


    public String getLine(int index) throws WindowException {
        if (lines == null) {
            throw new WindowException(WindowErrorCode.EMPTY_ARRAY);
        } else if (lines.length <= index|index < 0) {
            throw new WindowException(WindowErrorCode.WRONG_INDEX);
        }  else {
            return lines[index];
        }
    }


    public void setLine(int index, String line) throws WindowException {
        if (lines == null) {
            throw new WindowException(WindowErrorCode.EMPTY_ARRAY);
        } else if (lines.length <= index|index < 0) {
            throw new WindowException(WindowErrorCode.WRONG_INDEX);
        } else {
            String newArray[] = new String[lines.length];
            System.arraycopy(lines, 0, newArray, 0, lines.length);
            newArray[index] = line;
            lines = newArray;
        }
    }

    public Integer findLine(String line) {
        if (lines != null) {
            for (int i = 0; i < lines.length; i++) {
                if (lines[i].equals(line)) {
                    return i;
                }
            }
        }
        return null;
    }

    public void reverseLineOrder() {
        if (lines != null) {
            Collections.reverse(Arrays.asList(lines));
        }
    }

    public void reverseLines() {
        if (lines != null) {
            for (int i = 0; i < lines.length; i++) {
                lines[i] = new StringBuilder(lines[i]).reverse().toString();
            }
        }
    }

    public void duplicateLines() {
        if (lines != null) {
            String[] stringDestination = new String[lines.length * 2];
            int j = 0;
            for (int i = 0; i < stringDestination.length; i = i + 2) {
                stringDestination[i] = lines[j];
                stringDestination[i + 1] = lines[j];
                j++;
            }
            lines = stringDestination;
        }
    }

    public void removeOddLines() {
        if (lines != null) {
            int k;
            if (lines.length % 2 == 0) {
                k = lines.length / 2;
            } else {
                k = lines.length / 2 + 1;
            }
            String[] newArray = new String[k];
            int j = 0;
            for (int i = 0; i < lines.length; i += 2) {
                newArray[j] = lines[i];
                j++;
            }
            lines = newArray;
        }
    }

    public boolean isSortedDescendant() {
        if (lines != null) {
            for (int i = 0; i < lines.length - 1; i++) {
                if (lines[i].compareTo(lines[i + 1]) > 0) {
                    return true;
                }
            }
        } else {
            return true;
        }
        return false;
    }

    public void moveTo(int x, int y) {
        int width = getWidth() - 1;
        int height = getHeight() - 1;
        getTopLeft().moveTo(x, y);
        getBottomRight().moveTo(getTopLeft().getX() + width, getTopLeft().getY() + height);
    }

    public void moveTo(Point point) {
        this.moveTo(point.getX(), point.getY());
    }


    public void moveRel(int dx, int dy) {
        int width = getWidth() - 1;
        int height = getHeight() - 1;
        getTopLeft().moveTo(getTopLeft().getX() + dx, getTopLeft().getY() + dy);
        getBottomRight().moveTo(getTopLeft().getX() + width, getTopLeft().getY() + height);
    }

    public void resize(double ratio) {
        int width = (int) (getWidth() * ratio);
        int height = (int) (getHeight() * ratio);
        if (width < 1 || height < 1) {
            getBottomRight().setX(getTopLeft().getX());
            getBottomRight().setY(getTopLeft().getY());
        } else {
            getBottomRight().setX(getTopLeft().getX() + width - 1);
            getBottomRight().setY(getTopLeft().getY() + height - 1);
        }
    }

    public boolean isInside(int x, int y) {
        return (x >= getTopLeft().getX() &&
                y >= getTopLeft().getY() &&
                x <= getBottomRight().getX() &&
                y <= getBottomRight().getY());
    }

    public boolean isInside(Point point) {
        return isInside(point.getX(), point.getY());
    }

    public boolean isIntersects(ListBox listBox) {
        return isInside(listBox.getTopLeft())
                || isInside(listBox.getBottomRight())
                || listBox.isInside(getTopLeft())
                || listBox.isInside(getBottomRight());
    }

    public boolean isInside(ListBox listBox) {
        return (isInside(listBox.getTopLeft())
                && isInside(listBox.getBottomRight())
                || listBox.isInside(getTopLeft())
                && listBox.isInside(getBottomRight()));
    }

    public boolean isFullyVisibleOnDesktop(Desktop desktop) {
        return (getTopLeft().getX() >= 0
                && getBottomRight().getX() <= desktop.getWidth())
                && (getTopLeft().getY() >= 0
                && getBottomRight().getY() <= desktop.getHeight());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ListBox)) return false;
        if (!super.equals(o)) return false;
        ListBox listBox = (ListBox) o;
        return Arrays.equals(lines, listBox.lines);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Arrays.hashCode(lines);
        return result;
    }
}
