package ml.antanaks.school.windows.v4;

import ml.antanaks.school.windows.v4.base.RectWindow;
import ml.antanaks.school.windows.v4.base.WindowErrorCode;
import ml.antanaks.school.windows.v4.base.WindowException;
import ml.antanaks.school.windows.v4.base.WindowState;

import java.util.Objects;

public class RectButton extends RectWindow {

    private String text;

    public RectButton(Point topLeft, Point bottomRight, WindowState state, String text) throws WindowException {
        super(topLeft, bottomRight, state);
        this.text = text;
        if (state == null) {
            throw new WindowException(WindowErrorCode.WRONG_STATE);
        }
        if (state == WindowState.DESTROYED){
            throw new WindowException(WindowErrorCode.WRONG_STATE);
        }
    }

    public RectButton(int xLeft, int yTop, int width, int height, WindowState state, String text) throws WindowException {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), state, text);
    }

    public RectButton(int xLeft, int yTop, int width, int height, String state, String text) throws WindowException {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), WindowState.fromString(state), text);
    }

    public RectButton(Point topLeft, Point bottomRight, String text) throws WindowException {
        this(topLeft, bottomRight, WindowState.ACTIVE, text);
    }

    public RectButton(int xLeft, int yTop, int width, int height, String text) throws WindowException {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), WindowState.ACTIVE, text);
    }

    public RectButton(Point topLeft, Point bottomRight, String state, String text) throws WindowException {
        this(topLeft, bottomRight, WindowState.fromString(state), text);
    }

    public boolean isInside(int x, int y) {
        return (x >= getTopLeft().getX() &&
                y >= getTopLeft().getY() &&
                x <= getBottomRight().getX() &&
                y <= getBottomRight().getY());
    }

    public boolean isInside(Point point) {
        return isInside(point.getX(), point.getY());
    }

    public boolean isIntersects(RectButton rectButton) {
        return isInside(rectButton.getTopLeft())
                || isInside(rectButton.getBottomRight())
                || rectButton.isInside(getTopLeft())
                || rectButton.isInside(getBottomRight());
    }

    public boolean isInside(RectButton rectButton) {
        return (isInside(rectButton.getTopLeft())
                && isInside(rectButton.getBottomRight())
                || rectButton.isInside(getTopLeft())
                && rectButton.isInside(getBottomRight()));
    }

    public boolean isFullyVisibleOnDesktop(Desktop desktop) {
        return (getTopLeft().getX() >= 0
                && getBottomRight().getX() <= desktop.getWidth())
                && (getTopLeft().getY() >= 0
                && getBottomRight().getY() <= desktop.getHeight());
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RectButton that = (RectButton) o;
        return Objects.equals(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), text);
    }
}
