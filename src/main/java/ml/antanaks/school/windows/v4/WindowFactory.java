package ml.antanaks.school.windows.v4;

import ml.antanaks.school.windows.v4.base.WindowException;
import ml.antanaks.school.windows.v4.base.WindowState;

public class WindowFactory {

    static int rectButtonCount;
    static int roundButtonCount;


    public static RectButton createRectButton(Point leftTop, Point rightBottom, WindowState state, String text) throws WindowException {
        rectButtonCount++;
        return new RectButton(leftTop, rightBottom, state, text);

    }

    public static RoundButton createRoundButton(Point center, int radius, WindowState state, String text) throws WindowException {
        roundButtonCount++;
        return new RoundButton(center, radius, state, text);
    }

    public static int getRectButtonCount() {
        return rectButtonCount;
    }


    public static int getRoundButtonCount() {
        return roundButtonCount;
    }

    public static int getWindowCount() {
        return rectButtonCount + roundButtonCount;
    }

    public static void reset() {
        rectButtonCount = 0;
        roundButtonCount = 0;
    }
}
