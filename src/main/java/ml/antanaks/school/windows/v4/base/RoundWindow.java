package ml.antanaks.school.windows.v4.base;

import ml.antanaks.school.windows.v4.Desktop;
import ml.antanaks.school.windows.v4.Point;

import java.util.Objects;

public abstract class RoundWindow extends Window {

    private Point center;
    private int radius;
    private String text;

    public RoundWindow(Point center, int radius,WindowState state,String text) {
        super(state);
        this.center = center;
        this.radius = radius;
        this.text = text;
    }


    public void moveTo(int x, int y) {
        center.setX(x);
        center.setY(y);
    }

    public void moveRel(int dx, int dy) {
        center.setX(center.getX() + dx);
        center.setY(center.getY() + dy);
    }

    public void setCenter(int x, int y) {
        this.center = new Point(x, y);
    }

    public void resize(double ratio) {
        int radius1 = (int) (getRadius() * ratio);
        radius = Math.max(radius1, 1);
    }

    public boolean isInside(int x, int y) {
        double dis = Math.pow((center.getX() - x), 2) + Math.pow(center.getY() - y, 2);
        return ((int) Math.sqrt(dis)) <= radius;
    }

    public boolean isInside(Point point) {
        double dis = Math.pow(center.getX() - point.getX(), 2) + Math.pow(center.getY() - point.getY(), 2);
        return ((int) Math.sqrt(dis)) <= radius;
    }

    public boolean isFullyVisibleOnDesktop(Desktop desktop) {
        return ((center.getX() - radius) >= 0
                && (center.getX() + radius) < desktop.getWidth())
                && ((center.getY() - radius) >= 0
                && (center.getY() + radius) < desktop.getHeight());
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RoundWindow that = (RoundWindow) o;
        return radius == that.radius && Objects.equals(center, that.center) && Objects.equals(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), center, radius, text);
    }
}
