package ml.antanaks.school.windows.v4.base;

import ml.antanaks.school.windows.v4.Desktop;
import ml.antanaks.school.windows.v4.Point;
import ml.antanaks.school.windows.v4.iface.Movable;
import ml.antanaks.school.windows.v4.iface.Resizable;

import java.util.Objects;

public abstract class Window implements Movable, Resizable {

    private WindowState state;

    public Window(WindowState state){
        this.state = state;
    }

    public WindowState getState() {
        return state;
    }

    public void setState(WindowState state) throws WindowException {
        if (getState() == WindowState.DESTROYED) {
            throw new WindowException(WindowErrorCode.WRONG_STATE);
        } else if (state == WindowState.DESTROYED && getState() == null) {
            throw new WindowException(WindowErrorCode.WRONG_STATE);
        } else {
            this.state = state;
        }
    }


    public abstract boolean isInside(int x, int y);

    public abstract boolean isInside(Point point);

    public abstract boolean isFullyVisibleOnDesktop(Desktop desktop);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Window window = (Window) o;
        return state == window.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(state);
    }
}
