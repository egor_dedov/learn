package ml.antanaks.school.windows.v4.base;

public enum WindowErrorCode {
    WRONG_STATE("Что-то не так с состоянием кнопки!"),
    WRONG_INDEX("Неккоректный индекс"),
    EMPTY_ARRAY("Что-то не так с массивом");

    private String errorString;

    WindowErrorCode(String errorString) {
        this.errorString = errorString;
    }

    WindowErrorCode() {
    }

    public String getErrorString() {
        return errorString;
    }
}
