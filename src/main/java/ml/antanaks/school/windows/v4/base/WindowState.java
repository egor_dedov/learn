package ml.antanaks.school.windows.v4.base;

public enum WindowState {
    ACTIVE("Кнопка активна"),
    INACTIVE("Кнопка не активна" ),
    DESTROYED("Кнопка в состоянии дестрой");

    private String info;

    WindowState(String info){
        this.info = info;
    }

    public static WindowState fromString(String stateString) throws WindowException {
        WindowState state = null;
        if (stateString != null) {
            state = switch (stateString) {
                case "ACTIVE" -> ACTIVE;
                case "INACTIVE" -> INACTIVE;
                case "DESTROYED" -> DESTROYED;
                default -> throw new WindowException(WindowErrorCode.WRONG_STATE);
            }; // Если правильно задекларировать класс - можно сделать проще
        }
        return state;
    }
}
