package ml.antanaks.school.windows.v4.cursors;

import ml.antanaks.school.windows.v4.Point;
import ml.antanaks.school.windows.v4.iface.Movable;

public class Cursor implements Movable { //У нас есть класс который хранит переменную Х и У используй его вместо отдельных переменных

    private int x;
    private int y;
    private CursorForm cursorForm;

    public Cursor(int x, int y, CursorForm cursorForm) {
        this.x = x;
        this.y = y;
        this.cursorForm = cursorForm;
    }

    public Cursor(Point point, CursorForm cursorForm) {
        this.x = point.getX();
        this.y = point.getY();
        this.cursorForm = cursorForm;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public CursorForm getCursorForm() {
        return cursorForm;
    }

    public void setCursorForm(CursorForm cursorForm) {
        this.cursorForm = cursorForm;
    }

    @Override
    public void moveTo(int x, int y) {
        setX(x);
        setY(y);
    }

    @Override
    public void moveRel(int dx, int dy) {
        setX(getX() + dx);
        setY(getY() + dy);
    }
}
