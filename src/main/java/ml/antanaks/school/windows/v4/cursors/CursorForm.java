package ml.antanaks.school.windows.v4.cursors;

public enum CursorForm {
    ARROW("Стрелка на северо-запад"),
    UPARROW("Вертикальная стрелка"),
    CROSS("Перекрестие"),
    HELP("Стрелка - вопрос"),
    WAIT("Стрелка- ожидание");

    private String inf;

    CursorForm(String inf){
        this.inf = inf;
    }
}
