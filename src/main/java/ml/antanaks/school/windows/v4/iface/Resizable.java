package ml.antanaks.school.windows.v4.iface;

public interface Resizable {

    void resize(double ratio);

}
